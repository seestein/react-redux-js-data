
let proxy = (config) => ({
  enabled: true,
  options: {
    host: config.backend_rest_host,
    match: /^\/rest\/.*/,
    map: function(path) {
      if (/rest/.test(path)) {
        return path.replace('/rest', '');
      }
      return path;
    }
  }
});

export default {
  development: (config) => ({
    compiler_public_path: `http://${config.server_host}:${config.server_port}/`,
    compiler_stats: {
      chunks: false,
      chunkModules: false,
      colors: false
    },
    proxy: proxy(config)
  }),
  production: (config) => ({
    compiler_public_path: '/',
    compiler_fail_on_warning: false,
    compiler_hash_type: 'chunkhash',
    compiler_devtool: null,
    compiler_stats: {
      chunks: true,
      chunkModules: false,
      colors: true
    },
    proxy: proxy(config)
  })
}
