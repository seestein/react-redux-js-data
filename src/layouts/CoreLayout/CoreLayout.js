/**
 * Created by artemsitnikov on 07.08.16.
 */
import React, {Component} from 'react';
import { connect } from 'react-redux';
import storeJSData from '../../models/store';
import './CoreLayout.css'
import '../../static/css/normalize.css'
import '../../static/css/font-awesome.css'

class CoreLayout extends Component {
  render() {
    return (
      <main className="container">
        {this.props.children}
      </main>
    )
  }
}

CoreLayout.propTypes = {
  children: React.PropTypes.element.isRequired
};

CoreLayout.prototype.displayName = 'CoreLayout';

export default connect()(CoreLayout);
