/**
 * Created by artem on 20.08.16.
 */
export const MODEL_LOADED = 'MODEL_LOADED';
export const MODEL_DELETED = 'MODEL_DELETED';
export const MODEL_UPDATED = 'MODEL_UPDATED';