/**
 * Created by artem on 20.08.16.
 */
import * as types from './types';
import storeJSData from '../models/store';
import { normalize, Schema, arrayOf } from 'normalizr';
import { push } from 'react-router-redux';

export function modelLoaded(model, data) {
  return {
    type: types.MODEL_LOADED,
    model,
    data
  }
}

export function modelDeleted(model, id) {
  return {
    type: types.MODEL_DELETED,
    model,
    id
  }
}

export function modelUpdated(model, id, data) {
  return {
    type: types.MODEL_UPDATED,
    model,
    id,
    data
  }
}

function injectNormalizedData(model, results = []) {
  return dispatch => {
    let schema = {};
    let modelSchema = new Schema(model);
    schema[model] = arrayOf(modelSchema);
    dispatch(modelLoaded(model, normalize({[model]: results}, schema)));
    return Promise.resolve();
  }
}

export function fetchModelFromRest(model, id, params) {
  return dispatch => {

    //если есть id, то запрашиваем конкретную модель
    if (id) {
      return storeJSData.find(model, id, params).then(() => {
        let results = storeJSData.get(model, id);
        return dispatch(injectNormalizedData(model, [results]));
      });
    }

    return storeJSData.findAll(model, params).then(() => {
      let results = storeJSData.getAll(model);
      return dispatch(injectNormalizedData(model, results));
    });
  }
}

export function deleteModelFromRest(model, id) {
  return dispatch => {
    storeJSData.destroy(model, id).then(() => dispatch(modelDeleted(model, id)));
  }
}

export function saveModelToRest(model, id, data, params) {
  return dispatch => {
    //сохраняем новую модель на сервере
    if (id === 'new') {
      return storeJSData.create(model, data, params).then(() => {
        //саму временную модель с id=new удаляем
        dispatch(modelDeleted(model, id));
        return Promise.resolve();
      });
    }

    //обновляем модель на сервере и кеше
    return storeJSData.find(model, id, params).then(modelItem => {
      Object.assign(modelItem, data);
      return storeJSData.save(model, id, { cacheResponse: false });
    });
  }
}

export function startEditModel(model, id, parent_model, parent_id) {
  return dispatch => {
    let prefix = parent_model ? `/${parent_model}/${parent_id}` : '';
    dispatch(push(`${prefix}/${model}/${id || 'new'}`))
  }
}