/**
 * Created by artem on 21.08.16.
 */
import React, {Component} from 'react';
import EditForm from '../../containers/EditForm/EditForm';

// Sync route definition
export default {
  path: ':parent_model/:parent_id/:model/:id',
  component: EditForm
}
