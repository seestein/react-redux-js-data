/**
 * Created by artem on 19.08.16.
 */
import React, {Component} from 'react';
import GroupList from '../../containers/GroupList/GroupList';

// Sync route definition
export default {
  component: GroupList
}
