/**
 * Created by artemsitnikov on 07.08.16.
 */
import CoreLayout from '../layouts/CoreLayout/CoreLayout';
import GroupList from './GroupList';
import GroupNew from './GroupNew';
import ContactNew from './ContactNew';
import React, {Component} from 'react';

export const createRoutes = (store) => ({
  path: '/',
  component: CoreLayout,
  indexRoute: GroupList,
  childRoutes: [GroupNew, ContactNew]
});

export default createRoutes
