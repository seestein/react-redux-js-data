/**
 * Created by artem on 20.08.16.
 */
import * as types from '../actions/types';

export default function entities(state = {}, action) {
  let models;
  switch (action.type) {
  case types.MODEL_LOADED:
    //в существующих моделях обновлям только пришедшие с сервера поля
    models = {...action.data.entities[action.model]};
    models = Object.keys(models).reduce((models, modelId) => {
      models[modelId] = {
        ...(state[action.model] && state[action.model][modelId]),
        ...action.data.entities[action.model][modelId]
      };
      return models;
    }, {});

    return {
      ...state,
      [action.model]: {
        ...state[action.model],
        ...models
      }
    };
  case types.MODEL_DELETED:
    models = {...state[action.model]};
    delete models[action.id];

    return {
      ...state,
      [action.model]: models
    };
  case types.MODEL_UPDATED:
    return {
      ...state,
      [action.model]: {
        ...state[action.model],
        [action.id]: {...state[action.model] && state[action.model][action.id], ...action.data}
      }
    };
  default:
    return state
  }
}