/**
 * Created by artem on 21.08.16.
 */
import Schemator from 'js-data-schema';
import * as constants from '../utils/constants';

var schemator = new Schemator();

schemator.defineRule('required', function (x, isRequired) {
  if (x === '' && isRequired) {
    return {
      rule: 'required',
      msg: 'Обязательно для заполнения'
    };
  }
  return null;
});

schemator.defineSchema(constants.MODEL_GROUP, {
  name: {
    order: 0,
    type: 'string',
    title: 'Название группы',
    fieldName: 'name',
    required: true
  },
  description: {
    order: 1,
    type: 'string',
    title: 'Описание',
    fieldName: 'description',
    required: true
  }
});

schemator.defineSchema(constants.MODEL_CONTACT, {
  name: {
    order: 0,
    type: 'string',
    title: 'Название контакта',
    fieldName: 'name',
    required: true
  },
  phone: {
    order: 1,
    type: 'string',
    title: 'Телефон',
    fieldName: 'phone',
    required: true
  }
});

export default schemator;