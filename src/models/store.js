/**
 * Created by artem on 20.08.16.
 */
import JSData from 'js-data';
import DSHttpAdapter from 'js-data-http';

let storeJSData = new JSData.DS({
  afterInject: function (resource, instances) {
    if (!Array.isArray(instances)) {
      instances = [instances];
    }
    storeJSData.emit('DS.afterInject', resource, instances);
  }
});

storeJSData.registerAdapter('http', new DSHttpAdapter({
  basePath: 'http://localhost:3030/rest/',
  suffix: '.json',
  log: false
}));

storeJSData.defineResource({
  name: 'group',
  endpoint: 'groups',
  relations: {
    hasMany: {
      contact: [
        {
          localField: 'contact',
          foreignKey: 'group_id'
        }
      ]
    }
  }
});

storeJSData.defineResource({
  name: 'contact',
  endpoint: 'contacts',
  relations: {
    belongsTo: {
      group: {
        parent: true,
        localKey: 'group_id',
        localField: 'group'
      }
    }
  }
});

export default storeJSData;