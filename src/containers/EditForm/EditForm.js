/**
 * Created by artem on 21.08.16.
 */
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { fetchModelFromRest, modelUpdated, saveModelToRest } from '../../actions';
import * as constants from '../../utils/constants';
import EditForm from '../../components/EditForm/EditForm';

const mapDispatchToProps = { fetchModelFromRest, modelUpdated, saveModelToRest, push };

const mapStateToProps = (state, props) => {
  let { params: { model, id } } = props;
  let data = state.entities[model] && state.entities[model][id];
  return { data };
};

export default connect(mapStateToProps, mapDispatchToProps)(EditForm);