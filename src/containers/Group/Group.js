/**
 * Created by artem on 21.08.16.
 */
import { connect } from 'react-redux';
import { fetchModelFromRest, deleteModelFromRest, modelUpdated, startEditModel } from '../../actions';
import * as constants from '../../utils/constants';
import Group from '../../components/Group/Group';

const mapDispatchToProps = {
  fetchModelFromRest,
  onDelete: deleteModelFromRest,
  onAdd: startEditModel,
  onEdit: startEditModel,
  modelUpdated
};

const mapStateToProps = (state, props) => {
  let { data: { id: groupId } } = props;
  let contacts = Object.keys(state.entities[constants.MODEL_CONTACT] || {})
    .map(contactId => state.entities[constants.MODEL_CONTACT][contactId])
    .filter(contact => contact.group_id == groupId)
    .sort((a, b) => b.id - a.id);

  return {
    contacts
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Group);