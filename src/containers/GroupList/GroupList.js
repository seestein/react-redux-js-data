/**
 * Created by artem on 20.08.16.
 */
import { connect } from 'react-redux';
import { fetchModelFromRest } from '../../actions';
import * as constants from '../../utils/constants';
import GroupList from '../../components/GroupList/GroupList';

const mapDispatchToProps = { fetchModelFromRest };

const mapStateToProps = (state) => {
  let groups = Object.keys(state.entities[constants.MODEL_GROUP] || {})
    .map(groupId => state.entities[constants.MODEL_GROUP][groupId])
    .sort((a, b) => b.id - a.id);

  return { groups }
};

export default connect(mapStateToProps, mapDispatchToProps)(GroupList);