/**
 * Created by artem on 21.08.16.
 */
import React, {Component} from 'react';
import { Link } from 'react-router'
import schemator from '../../models/schema';
import './style.css';

function sortSchemaFields(schema) {
  return Object.keys(schema)
    .map(field => schema[field])
    .sort((a, b) => a.order - b.order);
}

export default class EditForm extends Component {

  constructor(props) {
    super(props);
    this.form = {};
  }

  componentDidMount() {
    let { params: { model, id, parent_model, parent_id } } = this.props;

    if (id === 'new') return;

    this.props.fetchModelFromRest(model, id, this.getParams());
  }

  getParams = () => {
    let { params: { parent_model, parent_id } } = this.props;
    return parent_model && { params: { [`${parent_model}_id`]: parent_id } };
  };

  handleSubmit = (e) => {
    e.preventDefault();
    let { params: { model, id, parent_model, parent_id } } = this.props;
    let data = Object.keys(this.form).reduce((data, field) => {
      data[field] = this.form[field].value;
      return data;
    }, {});

    //валидация данных
    let errors = schemator.getSchema(model).validateSync(data);

    //TODO супер простая проверка, так как на данный момент исхожу из соображений, что только один тип ошибки
    if (errors) {
      alert('Поля отмеченные символом * обязательны для заполнения');
    } else {
      this.props.saveModelToRest(model, id, data, this.getParams()).then(() => this.props.push('/'));
    }
  };

  handleChange = (field) => (e) => {
    let { params: { model, id } } = this.props;
    this.props.modelUpdated(model, id, {
      [field.fieldName]: e.target.value
    });
  };

  render() {
    let { params: { model }, data } = this.props;

    let schema = schemator.getSchema(model).schema;
    let fields = sortSchemaFields(schema);

    return (
      <form className="form-edit" onSubmit={this.handleSubmit} method="post">
        {fields.map((field, i) => {
          return (
            <div key={i} className="form-edit__field">
              <label className="form-edit__field-title">
                {field.title}
                {field.required && <span>*</span>}
              </label>
              <input className="form-edit__field-input" type="text"
                  ref={(ref) => this.form[field.fieldName] = ref}
                  name={field.fieldName}
                  value={data && data[field.fieldName] || ''}
                  onChange={this.handleChange(field)}
              />
            </div>
          )
        })}
        <div className="form-edit__buttons">
          <Link to="/">назад</Link>
          <input type="submit" value="Сохранить" />
        </div>
      </form>
    )
  }
}

EditForm.propTypes = {
  fetchModelFromRest: React.PropTypes.func,
  saveModelToRest: React.PropTypes.func,
  modelUpdated: React.PropTypes.func,
  push: React.PropTypes.func
};

EditForm.prototype.displayName = 'EditForm';