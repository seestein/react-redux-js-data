/**
 * Created by artem on 19.08.16.
 */
import React, {Component} from 'react';
import './style.css'

let defaultBtnTypes = {
  onAdd: {icon: 'fa-plus-circle'},
  onEdit: {icon: 'fa-edit'},
  onDelete: {icon: 'fa-remove'},
  onToggle: {icon: 'fa-angle-right'}
};

export default class Buttons extends Component {
  render() {
    let btnTypes = Object.assign({}, defaultBtnTypes, this.props.btnTypes);

    return (
      <div className="buttons-group clearfix">
        {Object.keys(btnTypes)
          .filter(type => this.props[type])
          .map((type, i) => {
            let classes = ['button-group__btn', 'fa'];
            classes.push(btnTypes[type].icon);
            return <div key={i} onClick={this.props[type]} className={classes.join(' ')} />;
          })
        }
      </div>
    )
  }
}

Buttons.propTypes = {
  onDelete: React.PropTypes.func,
  btnTypes: React.PropTypes.objectOf(React.PropTypes.object)
};

Buttons.prototype.displayName = 'Buttons';