/**
 * Created by artem on 20.08.16.
 */
import React, {Component} from 'react';
import Buttons from '../Buttons/Buttons/Buttons';
import * as constants from '../../utils/constants';
import './style.css';

export default class Contact extends Component {

  onDelete = () => {
    let { data: { id } } = this.props;
    this.props.onDelete(constants.MODEL_CONTACT, id);
  };

  onEdit = () => {
    let { data: { id, group_id } } = this.props;
    this.props.onEdit(constants.MODEL_CONTACT, id, constants.MODEL_GROUP, group_id);
  };

  render() {
    let { data: { name, phone} } = this.props;

    return (
      <div className="contact-item clearfix">
        <div className="contact-item__name">{name}</div>
        <div className="contact-item__phone">{phone}</div>
        <div className="contact-item__buttons">
          <Buttons
              onEdit={this.onEdit}
              onDelete={this.onDelete}
          />
        </div>
      </div>
    )
  }
}

Contact.propTypes = {
  onDelete: React.PropTypes.func,
  onEdit: React.PropTypes.func
};

Contact.prototype.displayName = 'Contact';