/**
 * Created by artem on 19.08.16.
 */
import React, {Component} from 'react';
import { Link } from 'react-router'
import Group from '../../containers/Group/Group';
import * as constants from '../../utils/constants';
import './style.css';

export default class GroupList extends Component {

  componentDidMount() {
    this.props.fetchModelFromRest(constants.MODEL_GROUP);
  }

  render() {
    let { groups } = this.props;

    return (
      <div className="group-list">
        {groups.map((group, i) => <Group key={i} data={group} />)}
        <Link to={`/${constants.MODEL_GROUP}/new`}>Добавить новую группу</Link>
      </div>
    )
  }
}

GroupList.propTypes = {
  fetchModelFromRest: React.PropTypes.func,
  groups: React.PropTypes.arrayOf(React.PropTypes.object)
};

GroupList.prototype.displayName = 'GroupList';