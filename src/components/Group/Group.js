/**
 * Created by artem on 19.08.16.
 */
import React, {Component} from 'react';
import Buttons from '../Buttons/Buttons/Buttons';
import Contact from '../Contact/Contact';
import * as constants from '../../utils/constants';
import './style.css';

export default class Group extends Component {

  componentDidMount() {
    let { data: { id, isOpened } } = this.props;
    if (isOpened) {
      this.props.fetchModelFromRest(constants.MODEL_CONTACT, null, { group_id: id });
    }
  }

  onAdd = () => {
    let { data: { id } } = this.props;
    this.props.onAdd(constants.MODEL_CONTACT, null, constants.MODEL_GROUP, id);
  };

  onDelete = () => {
    let { data: { id } } = this.props;
    this.props.onDelete(constants.MODEL_GROUP, id);
  };

  onEdit = () => {
    let { data: { id } } = this.props;
    this.props.onEdit(constants.MODEL_GROUP, id);
  };

  onToggle = () => {
    let { data: { id, isOpened } } = this.props;

    if (!isOpened) {
      this.props.fetchModelFromRest(constants.MODEL_CONTACT, null, {  group_id: id });
    }

    this.props.modelUpdated(constants.MODEL_GROUP, id, { isOpened: !isOpened });
  };

  render() {
    let { data: { name, description, isOpened }, contacts } = this.props;

    return (
      <div className={`contact-group ${!isOpened ? 'contact-group_closed' : ''}`}>
        <div className="contact-group__name">{name}</div>
        <div className="contact-group__description">
          <div className="contact-group__description-title">
            {description}
          </div>
          <div className="contact-group__buttons">
            <Buttons
                onAdd={this.onAdd}
                onEdit={this.onEdit}
                onDelete={this.onDelete}
                onToggle={this.onToggle}
                btnTypes={{
                  onToggle: {icon: isOpened ? 'fa-angle-down' : 'fa-angle-right'}
                }}
            />
          </div>
        </div>
        {isOpened &&
          <div className="contact-group__contacts">
            {contacts.length > 0 && contacts.map((contact, i) => (
              <Contact {...this.props} key={i} data={contact} />
            )) || <p>Список контактов пуст</p>}
          </div>
        }
      </div>
    )
  }
}

Group.propTypes = {
  fetchModelFromRest: React.PropTypes.func.isRequired,
  onAdd: React.PropTypes.func.isRequired,
  onEdit: React.PropTypes.func.isRequired,
  onDelete: React.PropTypes.func.isRequired,
  modelUpdated: React.PropTypes.func.isRequired
};

Group.prototype.displayName = 'Group';