/**
 * Created by artem on 22.08.16.
 */
import * as actions from '../../src/actions';
import * as types from '../../src/actions/types';

describe('(Action Creator) modelLoaded', () => {
  it('Should be exported as a function.', () => {
    expect(actions.modelLoaded).to.be.a('function')
  });

  it('Should return an action with type "MODEL_LOADED".', () => {
    expect(actions.modelLoaded()).to.have.property('type', types.MODEL_LOADED)
  });

  it('Should assign the first argument to the "model" property.', () => {
    expect(actions.modelLoaded('group')).to.have.property('model', 'group')
  });
});